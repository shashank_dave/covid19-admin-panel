const {
  override,
  addDecoratorsLegacy,
  disableEsLint,
  addWebpackAlias,
  fixBabelImports,
} = require("customize-cra");
const path = require("path");

module.exports = override(
  // enable legacy decorators babel plugin
  addDecoratorsLegacy(),

  // disable eslint in webpack
  disableEsLint(),

  // add webpack bundle visualizer if BUNDLE_VISUALIZE flag is enabled
  // process.env.BUNDLE_VISUALIZE == 1 && addBundleVisualizer(),

  // add an alias for "ag-grid-react" imports
  addWebpackAlias({
    ["@Components"]: path.resolve(__dirname, "src/components"),
    ["@Pages"]: path.resolve(__dirname, "src/pages"),
    ["@Prelogin"]: path.resolve(__dirname, "src/pages/pre-login"),
    ["@Postlogin"]: path.resolve(__dirname, "src/pages/post-login"),
    ["@Api"]: path.resolve(__dirname, "src/api"),
    ["@Services"]: path.resolve(__dirname, "src/services"),
    ["@Stores"]: path.resolve(__dirname, "src/stores"),
  }),

  // antd babel fixes
  fixBabelImports("import", {
    libraryName: "antd",
    style: "css",
  }),
  fixBabelImports("react-app-rewire-mobx", {
    libraryDirectory: "",
    camel2DashComponentName: false,
  })
);
