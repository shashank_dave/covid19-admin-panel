import { observable, action, computed } from "mobx";
import FetchBase from "@Services/FetchBase";
import history from "@Services/History";

class AuthStore extends FetchBase {
  @observable isUserAuthenticated = false;

  @action.bound
  authenticateUser = () => {
    localStorage.setItem("token", "thisismytoken");
    this.validateTokenAndLogin();
  };

  @action.bound
  validateTokenAndLogin = () => {
    const tokenFromLs = localStorage.getItem("token");
    if (tokenFromLs && tokenFromLs.length) {
      this.isUserAuthenticated = true;
      history.push("/");
    } else {
      this.isUserAuthenticated = false;
    }
  };

  @computed
  get checkIsUserAuthenticated() {
    if (this.isUserAuthenticated) return true;
    return false;
  }

  @action.bound
  logoutUser = () => {
    localStorage.setItem("token", "");
    this.isUserAuthenticated = false;
    history.push("/");
  };

  fetchRecord = async (url) => {
    try {
      const response = await this._get(url);
    } catch (e) {
      console.log("Error store: ", e);
    }
  };
}

export default AuthStore;
