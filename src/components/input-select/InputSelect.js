import React from "react";
import { Select } from "antd";

const { Option } = Select;

class InputSelect extends React.Component {
  render() {
    const { options, initValue, onValChange } = this.props;
    return (
      <Select
        defaultValue={initValue}
        style={{ width: 180 }}
        onChange={(opt) => onValChange(opt)}
      >
        {options.length &&
          options.map((opt) => {
            return (
              <Option value={JSON.stringify(opt.location)} key={opt.id}>
                {opt.name}
              </Option>
            );
          })}
      </Select>
    );
  }
}

export default InputSelect;
