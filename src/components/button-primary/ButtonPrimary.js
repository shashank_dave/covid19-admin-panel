import React from "react";
import { Button } from "antd";

import "./ButtonPrimary.scss";

class ButtonPrimary extends React.Component {
  render() {
    return (
      <div className="primary-button-panel">
        <Button {...this.props} className="primary-button">
          {this.props.children}
        </Button>
      </div>
    );
  }
}

export default ButtonPrimary;
