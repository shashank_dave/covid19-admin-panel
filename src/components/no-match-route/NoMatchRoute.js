import React from "react";
import { useLocation, Redirect } from "react-router-dom";

export default function NoMatch() {
  // let location = useLocation();
  return <Redirect to="/" />;
}
