import React from "react";
import { Layout, Avatar, Popover } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";

const { Header } = Layout;

class PostLayoutHeader extends React.Component {
  render() {
    const { toggle, collapsed, onLogout } = this.props;
    const content = (
      <div>
        <p onClick={onLogout}>Logout</p>
      </div>
    );

    return (
      <Header className="site-layout-background" style={{ padding: 0 }}>
        {React.createElement(
          collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
          {
            className: "trigger",
            onClick: toggle,
          }
        )}
        <span style={{ float: "right", paddingRight: "20px" }}>
          <Popover placement="bottomLeft" content={content} trigger="click">
            <Avatar
              src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
              size={40}
            />
          </Popover>
        </span>
      </Header>
    );
  }
}

export default PostLayoutHeader;
