import React from "react";
import { Layout, Menu } from "antd";

import siderMenu from "./siderMenu";

const { Sider } = Layout;

class PostLayoutSider extends React.Component {
  render() {
    const { collapsed } = this.props;
    return (
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <div className="logo" />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          {siderMenu.map((menu) => (
            <Menu.Item key={menu.id}>
              {menu.icon}
              <span>{menu.label}</span>
            </Menu.Item>
          ))}
        </Menu>
      </Sider>
    );
  }
}

export default PostLayoutSider;
