import React from "react";
import {
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
} from "@ant-design/icons";

export default [
  {
    id: 1,
    label: "Dashboard",
    path: "/",
    icon: <UserOutlined />,
  },
  {
    id: 2,
    label: "User Management",
    path: "/",
    icon: <VideoCameraOutlined />,
  },
  {
    id: 3,
    label: "Settings",
    path: "/",
    icon: <UploadOutlined />,
  },
];
