import React from "react";
import { Input } from "antd";

import "./InputText.scss";

class InputText extends React.Component {
  render() {
    return (
      <div className="input-text">
        <Input {...this.props} />
      </div>
    );
  }
}

export default InputText;
