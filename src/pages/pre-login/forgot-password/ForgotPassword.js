import React from "react";
import { Card, Avatar, Form } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

import InputText from "@Components/input-text/InputText";
import ButtonPrimary from "@Components/button-primary/ButtonPrimary";
import "./ForgotPassword.scss";

class ForgotPassword extends React.Component {
  onFinish = (values) => {
    console.log("Received values of form: ", values);
  };

  render() {
    return (
      <div className="forgot-pass-form-card">
        <Card
          style={{
            width: 300,
            minWidth: 400,
            textAlign: "center",
            boxShadow: "5px 5px #f0f1f2",
            borderRadius: "4px",
          }}
        >
          <span className="forgot-pass-form-avatar">
            <Avatar size={64} icon={<UserOutlined />} />
            <span>
              <h2>Forgot Password</h2>
            </span>
          </span>
          <Form
            name="normal_login"
            className="forgot-pass-form"
            initialValues={{ remember: true }}
            onFinish={this.onFinish}
          >
            <Form.Item
              name="email"
              rules={[
                {
                  type: "email",
                  message: "The input is not valid E-mail!",
                },
                {
                  required: true,
                  message: "Please input your E-mail!",
                },
              ]}
            >
              <InputText
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="E-mail"
              />
            </Form.Item>

            <Form.Item>
              <Link to="/" className="forgot-pass-form-forgot">
                Back to Login
              </Link>
            </Form.Item>

            <Form.Item>
              <ButtonPrimary type="primary" htmlType="submit">
                Reset
              </ButtonPrimary>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  }
}

export default ForgotPassword;
