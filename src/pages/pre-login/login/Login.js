import React from "react";
import { Card, Avatar, Form } from "antd";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";
import { UserOutlined, LockOutlined } from "@ant-design/icons";

import InputText from "@Components/input-text/InputText";
import ButtonPrimary from "@Components/button-primary/ButtonPrimary";
import "./Login.scss";

@inject("authStore")
@observer
class Login extends React.Component {
  componentDidMount() {
    const { authStore } = this.props;
    authStore.fetchRecord("/todos/1");
  }

  onFinish = (values) => {
    const { authStore } = this.props;
    authStore.authenticateUser();
  };

  render() {
    return (
      <div className="login-form-card">
        <Card
          style={{
            width: 300,
            minWidth: 400,
            textAlign: "center",
            boxShadow: "5px 5px #f0f1f2",
            borderRadius: "4px",
          }}
        >
          <span className="login-form-avatar">
            <Avatar size={64} icon={<UserOutlined />} />
            <span>
              <h2>Login</h2>
            </span>
          </span>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{ remember: true }}
            onFinish={this.onFinish}
          >
            <Form.Item
              name="email"
              rules={[
                {
                  type: "email",
                  message: "The input is not valid E-mail!",
                },
                {
                  required: true,
                  message: "Please input your E-mail!",
                },
              ]}
            >
              <InputText
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="E-mail"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your Password!" },
              ]}
            >
              <InputText
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>
            <Form.Item>
              {/* <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item> */}

              <Link to="/forgot-password" className="login-form-forgot">
                Forgot password
              </Link>
            </Form.Item>

            <Form.Item>
              <ButtonPrimary type="primary" htmlType="submit">
                Log in
              </ButtonPrimary>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  }
}

export default Login;
