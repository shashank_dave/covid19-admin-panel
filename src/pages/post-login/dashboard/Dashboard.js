import React from "react";
import { Map, InfoWindow, Marker, GoogleApiWrapper } from "google-maps-react";
import InputSelect from "@Components/input-select/InputSelect";
import locationOptions from "@Services/MapLocations.json";
import "./Dashboard.scss";

const LoadingContainer = (props) => <div>Fancy loading container!</div>;

class Dashboard extends React.Component {
  state = {
    currentLocation: JSON.stringify(locationOptions[0].location),
  };

  handleLocationChange = (loc) => {
    this.setState({ currentLocation: loc });
  };

  render() {
    let parsedLoc = JSON.parse(this.state.currentLocation);

    return (
      <div className="map-panel">
        <div className="location-selector-cont">
          <InputSelect
            options={locationOptions}
            onValChange={this.handleLocationChange}
            initValue={this.state.currentLocation}
          />
        </div>
        <div className="map-cont">
          <Map
            google={this.props.google}
            zoom={3}
            initialCenter={{
              lat: parsedLoc.lat,
              lng: parsedLoc.lng,
            }}
            center={{
              lat: parsedLoc.lat,
              lng: parsedLoc.lng,
            }}
            style={{ height: 450, position: "relative" }}
          >
            <Marker
              onClick={() => {}}
              name={"Current location"}
              position={{ lat: 37.5, lng: -79.0 }}
            />
            <Marker
              onClick={() => {}}
              name={"Current location"}
              position={{ lat: 28.1333328, lng: -81.625664164 }}
            />

            <InfoWindow onClose={() => {}}>
              <div>{/* <h1>{this.state.selectedPlace.name}</h1> */}</div>
            </InfoWindow>
          </Map>
        </div>
      </div>
    );
  }
}

export default GoogleApiWrapper({
  apiKey: "AIzaSyAwp_AUU0QkSFAbR09PwqYDWlIOHcPtMRE",
  LoadingContainer: LoadingContainer,
})(Dashboard);
