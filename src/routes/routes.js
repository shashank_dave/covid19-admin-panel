import React, { Suspense } from "react";

const Login = React.lazy(() => import("@Prelogin/login/Login.js"));
const Dashboard = React.lazy(() => import("@Postlogin/dashboard/Dashboard.js"));
const NoMatch = React.lazy(() =>
  import("@Components/no-match-route/NoMatchRoute")
);
const ForgotPassword = React.lazy(() =>
  import("@Prelogin/forgot-password/ForgotPassword.js")
);

export const publicRoutes = [
  {
    id: 1_1,
    path: "/login",
    type: "public",
    exact: true,
    component: Login,
  },
  {
    id: 1_2,
    path: "/",
    type: "public",
    exact: true,
    component: Login,
  },
  {
    id: 1_4,
    path: "/forgot-password",
    type: "public",
    exact: true,
    component: ForgotPassword,
  },
  {
    id: 1_3,
    path: "*",
    type: "public",
    component: NoMatch,
  },
];

export const privateRoutes = [
  {
    id: 2_1,
    path: "/dashboard",
    type: "private",
    exact: true,
    component: Dashboard,
  },
  {
    id: 2_2,
    path: "/",
    type: "private",
    exact: true,
    component: Dashboard,
  },
  {
    id: 2_3,
    path: "*",
    type: "private",
    component: NoMatch,
  },
];
