import axios from "axios";

class FetchBase {
  constructor() {
    this.configInterceptors();
  }

  configInterceptors = () => {
    axios.defaults.baseURL = process.env.REACT_APP_BASE_URL_DEV;

    axios.interceptors.request.use(
      function (config) {
        // console.log("config", config);
        // config.headers.common["Authorization"] = "AUTH_TOKEN";
        // Do something before request is sent
        return config;
      },
      function (error) {
        // Do something with request error
        console.log("Error in sending request: ", error);
        return Promise.reject(error);
      }
    );
  };

  requestHandler = (request) => {
    request.headers.common["Authorization"] = "AUTH_TOKEN";
    return request;
  };

  _get = async (url) => {
    try {
      const response = await axios.get(url);
      console.log("Response", response);
      return response;
    } catch (e) {
      console.log("Error: ", e);
    }
  };

  _post = async (url, params) => {
    try {
      const response = await axios.post(url, params);
      return response;
    } catch (e) {
      console.log("Error: ", e);
    }
  };
}

export default FetchBase;
